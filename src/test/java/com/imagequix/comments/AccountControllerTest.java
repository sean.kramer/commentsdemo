package com.imagequix.comments;

import com.imagequix.comments.dtos.AccountInput;
import com.imagequix.comments.exceptions.ApiError;
import com.imagequix.comments.models.Account;
import com.imagequix.comments.models.Comment;
import com.imagequix.comments.models.Post;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class AccountControllerTest extends AbstractControllerTest {

    public static final String TEST_ACCOUNT_ID = "8d1e3937-9faf-3e92-8862-b465ed3e53e6";

    @Test
    void testCreateAccount() {
        AccountInput accountInput = new AccountInput("john.doe@example.com", "John Doe");
        var account = apiPost("/accounts", accountInput, HttpStatus.CREATED, Account.class);
        assertEquals(accountInput.getName(), account.getName());
        assertNotNull(account.getId());
    }

    @Test
    void testLookupByEmail() {
        Account testAccount = generateAccount();
        var account = apiGet("/accounts/" + testAccount.getEmail(), HttpStatus.OK, Account.class);
        assertEquals(testAccount.getId(), account.getId());
        var error = apiGet("/accounts/fakeemail@example.com", HttpStatus.NOT_FOUND, ApiError.class);
        assertApiErrorContains(error, "not found");
    }

    @Test
    void testGetAccountPosts() {
        Account account1 = generateAccount();
        List<Post> posts1 = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            posts1.add(generatePost(account1));
        }
        Account account2 = generateAccount();
        List<Post> posts2 = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            posts2.add(generatePost(account2));
        }
        var page1 = apiGetPage("/accounts/" + account1.getId() + "/posts", HttpStatus.OK, Post.class, null);
        assertEquals(posts1.size(), page1.getContent().size());
        var page2 = apiGetPage("/accounts/" + account2.getId() + "/posts", HttpStatus.OK, Post.class, null);
        assertEquals(posts2.size(), page2.getContent().size());

        // test pagination
        var smallPage = apiGetPage("/accounts/" + account2.getId() + "/posts", HttpStatus.OK, Post.class, PageRequest.of(0, 1));
        assertEquals(1, smallPage.getSize());
        assertEquals(posts2.size(), smallPage.getTotalPages());
    }

    @Test
    void testGetAccountComments() {
        Account account1 = generateAccount();
        Account account2 = generateAccount();
        Post post1 = generatePost(account1);
        Post post2 = generatePost(account1);

        // This makes 10 top-level comments by each account, 5 on each post
        List<Comment> topLevel = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            topLevel.add(generateTopLevelComment(post1, account1));
            topLevel.add(generateTopLevelComment(post1, account2));
            topLevel.add(generateTopLevelComment(post2, account1));
            topLevel.add(generateTopLevelComment(post2, account2));
        }

        // This makes each user reply to every top-level comment (20 replies each)
        topLevel.forEach(comment -> {
            generateReplyComment(comment, account1);
            generateReplyComment(comment, account2);
        });

        // One extra post so accounts 1 and 2 don't have the same number of comments
        generateTopLevelComment(post1, account1);

        // Should see 30 comments by a single user
        var page = apiGetPage("/accounts/" + account1.getId() + "/comments", HttpStatus.OK, Comment.class, PageRequest.of(0, 50));
        assertEquals(31, page.getTotalElements());
    }
}
