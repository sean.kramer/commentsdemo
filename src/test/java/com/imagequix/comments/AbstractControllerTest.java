package com.imagequix.comments;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.imagequix.comments.exceptions.ApiError;
import com.imagequix.comments.models.Account;
import com.imagequix.comments.models.Comment;
import com.imagequix.comments.models.Post;
import com.imagequix.comments.repositories.AccountRepository;
import com.imagequix.comments.repositories.CommentRepository;
import com.imagequix.comments.repositories.PostRepository;
import com.imagequix.comments.services.CommentService;
import com.imagequix.comments.util.RestPage;
import io.zonky.test.db.AutoConfigureEmbeddedDatabase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles({"test"})
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@AutoConfigureEmbeddedDatabase(refresh = AutoConfigureEmbeddedDatabase.RefreshMode.AFTER_EACH_TEST_METHOD)
public abstract class AbstractControllerTest {

    public static final String TEST_USER_ID = "8d1e3937-9faf-3e92-8862-b465ed3e53e6";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private CommentService commentService;

    protected <T> Page<T> apiGetPage(String url, HttpStatus expectedStatus, Class<T> returnType, PageRequest pageRequest) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        if (pageRequest != null) {
            params.add("page", String.valueOf(pageRequest.getPageNumber()));
            params.add("size", String.valueOf(pageRequest.getPageSize()));
            if (pageRequest.getSort().isSorted()) {
                pageRequest.getSort().forEach(order -> params.add("sort", order.getProperty() + "," + order.getDirection()));
            }
        }
        var pageType = objectMapper.getTypeFactory().constructParametricType(RestPage.class, returnType);
        return apiRequest(url, GET, null, expectedStatus, objectMapper.constructType(pageType), params);
    }

    protected <T> T apiGet(String url, HttpStatus expectedStatus, Class<T> returnType) {
        return apiRequest(url, GET, null, expectedStatus, objectMapper.constructType(returnType), null);
    }

    protected <T> T apiPost(String url, Object requestBody, HttpStatus expectedStatus, Class<T> returnType) {
        return apiRequest(url, POST, requestBody, expectedStatus, objectMapper.constructType(returnType), null);
    }

    protected <T> T apiPut(String url, Object requestBody, HttpStatus expectedStatus, Class<T> returnType) {
        return apiRequest(url, PUT, requestBody, expectedStatus, objectMapper.constructType(returnType), null);
    }

    protected <T> T apiDelete(String url, HttpStatus expectedStatus, Class<T> returnType) {
        return apiRequest(url, DELETE, null, expectedStatus, objectMapper.constructType(returnType), null);
    }

    protected <T> T apiRequest(String url, HttpMethod method, Object requestBody, HttpStatus expectedStatus,
                               JavaType returnType, MultiValueMap<String, String> queryParams) {
        try {
            MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.request(method, url);
            requestBuilder.contentType(MediaType.APPLICATION_JSON);
            if (queryParams != null) {
                requestBuilder.queryParams(queryParams);
            }
            if (requestBody != null) {
                requestBuilder.content(objectMapper.writeValueAsString(requestBody));
            }
            MvcResult result = mockMvc.perform(requestBuilder).andExpect(status().is(expectedStatus.value())).andReturn();
            String responseBody = result.getResponse().getContentAsString();
            if (returnType == null || Void.class == returnType.getRawClass()) {
                if (StringUtils.hasText(responseBody)) {
                    throw new RuntimeException("Unexpected response body: " + responseBody);
                }
                return null;
            }
            return objectMapper.readValue(responseBody, returnType);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @BeforeEach
    void setUp() {
        accountRepository.save(new Account(UUID.fromString(TEST_USER_ID), "test@example.com", "Test User"));
    }

    protected Account generateAccount() {
        Account account = new Account();
        account.setEmail(account.getId() + "@example.com");
        account.setName(account.getId().toString());
        return accountRepository.save(account);
    }

    protected Post generatePost(Account account) {
        Post post = new Post();
        post.setAccount(account);
        post.setContent("Content " + post.getId());
        post.setTitle("Title " + post.getId());
        return postRepository.save(post);
    }

    protected Comment generateTopLevelComment(Post post, Account account) {
        Comment comment = new Comment();
        comment.setAccount(account);
        comment.setContent("Random Top-Level Comment " + comment.getId());
        comment.setPost(post);
        comment.setPostId(post.getId());
        return commentRepository.save(comment);
    }

    protected Comment generateReplyComment(Comment parent, Account account) {
        return commentService.create(null, parent.getId(), "Random Comment " + UUID.randomUUID(), account);
    }

    protected Account getTestAccount() {
        return accountRepository.findById(UUID.fromString(TEST_USER_ID)).orElseThrow();
    }

    protected void assertApiErrorContains(ApiError error, String key) {
        String normalKey = key.toLowerCase();
        assertTrue(error.errors().stream().anyMatch(e -> e.toLowerCase().contains(normalKey)));
    }

}
