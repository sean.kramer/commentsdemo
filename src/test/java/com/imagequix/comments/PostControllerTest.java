package com.imagequix.comments;

import com.imagequix.comments.dtos.AccountInput;
import com.imagequix.comments.dtos.CommentInput;
import com.imagequix.comments.dtos.PostInput;
import com.imagequix.comments.exceptions.ApiError;
import com.imagequix.comments.models.Account;
import com.imagequix.comments.models.Comment;
import com.imagequix.comments.models.Post;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class PostControllerTest extends AbstractControllerTest {

    @Test
    @WithMockUser(username = TEST_USER_ID)
    void testCreatePost() {
        PostInput postInput = new PostInput("New Post", "Content of the new post");
        var post = apiPost("/posts", postInput, HttpStatus.CREATED, Post.class);
        assertEquals(postInput.getTitle(), post.getTitle());
        assertEquals(postInput.getContent(), post.getContent());
        assertNotNull(post.getId());
        assertEquals(TEST_USER_ID, post.getAccount().getId().toString());
    }

    @Test
    void testCreatePostNoAuth() {
        PostInput postInput = new PostInput("New Post", "Content of the new post");
        var error = apiPost("/posts", postInput, HttpStatus.UNAUTHORIZED, ApiError.class);
        assertApiErrorContains(error, "no authorization");
    }

    @Test
    void testGetPost() {
        Post existingPost = generatePost(generateAccount());
        Post post = apiGet("/posts/" + existingPost.getId(), HttpStatus.OK, Post.class);
        assertEquals(existingPost.getId(), post.getId());
        assertEquals(existingPost.getTitle(), post.getTitle());
        assertEquals(existingPost.getContent(), post.getContent());
    }

    @Test
    void testGetCompletePost() {
        Account account = generateAccount();
        Post post = generatePost(account);
        Comment comment = generateTopLevelComment(post, account);
        Comment reply = generateReplyComment(comment, account);

        Post fetchedPost = apiGet("/posts/" + post.getId() + "/complete", HttpStatus.OK, Post.class);

        assertEquals(post.getId(), fetchedPost.getId());
        assertEquals(post.getTitle(), fetchedPost.getTitle());
        assertEquals(post.getContent(), fetchedPost.getContent());
        assertEquals(1, fetchedPost.getComments().size());
        assertEquals(comment.getId(), fetchedPost.getComments().get(0).getId());
        assertEquals(1, fetchedPost.getComments().get(0).getComments().size());
        assertEquals(reply.getId(), fetchedPost.getComments().get(0).getComments().get(0).getId());
    }

    @Test
    @WithMockUser(username = TEST_USER_ID)
    void testUpdatePost() {
        Post myPost = generatePost(getTestAccount());
        PostInput update = new PostInput("Updated Title", "Updated content.");
        Post updatedPost = apiPut("/posts/" + myPost.getId(), update, HttpStatus.OK, Post.class);
        assertEquals(update.getTitle(), updatedPost.getTitle());
        assertEquals(update.getContent(), updatedPost.getContent());
    }

    @Test
    @WithMockUser(username = TEST_USER_ID)
    void testUpdateUnownedPost() {
        Post myPost = generatePost(generateAccount());
        PostInput update = new PostInput("Updated Title", "Updated content.");
        ApiError error = apiPut("/posts/" + myPost.getId(), update, HttpStatus.FORBIDDEN, ApiError.class);
        assertApiErrorContains(error, "does not have access");
    }

    @Test
    @WithMockUser(username = TEST_USER_ID)
    void testDeletePost() {
        Post existingPost = generatePost(getTestAccount());
        Comment comment = generateTopLevelComment(existingPost, getTestAccount());

        apiDelete("/posts/" + existingPost.getId(), HttpStatus.NO_CONTENT, Void.class);
        var error = apiGet("/posts/" + existingPost.getId(), HttpStatus.NOT_FOUND, ApiError.class);
        assertApiErrorContains(error, "not found");

        // Ensure cascade delete worked
        apiGet("/comments/" + comment.getId(), HttpStatus.NOT_FOUND, ApiError.class);
    }

    @Test
    void testGetPosts() {
        generatePost(generateAccount());
        generatePost(generateAccount());
        Page<Post> posts = apiGetPage("/posts", HttpStatus.OK, Post.class, null);
        assertEquals(2, posts.getTotalElements());
        assertEquals(20, posts.getSize()); // Ensure default page size is respected
        posts = apiGetPage("/posts", HttpStatus.OK, Post.class, PageRequest.of(1, 500));
        assertEquals(0, posts.getNumberOfElements());
        assertEquals(50, posts.getSize()); // Ensure max page size is respected
    }

    @Test
    void testGetPostComments() {
        Post post = generatePost(generateAccount());
        Comment comment = generateTopLevelComment(post, generateAccount());
        // Extra unrelated comment to ensure it's not picked up
        generateTopLevelComment(generatePost(generateAccount()), generateAccount());

        Page<Comment> comments = apiGetPage("/posts/" + post.getId() + "/comments", HttpStatus.OK, Comment.class, null);
        assertEquals(1, comments.getTotalElements());
        assertEquals(comment.getId(), comments.getContent().get(0).getId());
    }

    @Test
    @WithMockUser(username = TEST_USER_ID)
    void testAddReply() {
        Post post = generatePost(getTestAccount());
        CommentInput commentInput = new CommentInput("Reply content");
        Comment reply = apiPost("/posts/" + post.getId() + "/comments", commentInput, HttpStatus.CREATED, Comment.class);
        assertEquals(commentInput.getContent(), reply.getContent());
        assertEquals(post.getId(), reply.getPostId());
        assertNotNull(reply.getId());
    }

    @Test
    void testReplyWithNoAuth() {
        Post post = generatePost(generateAccount());
        CommentInput commentInput = new CommentInput("Reply content");
        var error = apiPost("/posts/" + post.getId() + "/comments", commentInput, HttpStatus.UNAUTHORIZED, ApiError.class);
        assertApiErrorContains(error, "no authorization");
    }

    @Test
    @WithMockUser(username = "35635661-4aca-3f59-8a80-123b835b10cb") // This user isn't registered
    void testReplyWithInvalid() {
        Post post = generatePost(generateAccount());
        CommentInput commentInput = new CommentInput("Reply content");
        var error = apiPost("/posts/" + post.getId() + "/comments", commentInput, HttpStatus.UNAUTHORIZED, ApiError.class);
        assertApiErrorContains(error, "invalid token");
    }

}
