package com.imagequix.comments;

import com.imagequix.comments.dtos.CommentInput;
import com.imagequix.comments.exceptions.ApiError;
import com.imagequix.comments.models.Account;
import com.imagequix.comments.models.Comment;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CommentControllerTest extends AbstractControllerTest {

    @Test
    @WithMockUser(username = TEST_USER_ID)
    void testAddReply() {
        Account account = getTestAccount();
        Comment parentComment = generateTopLevelComment(generatePost(account), account);
        CommentInput replyInput = new CommentInput("This is a reply.");
        Comment reply = apiPost("/comments/" + parentComment.getId(), replyInput, HttpStatus.CREATED, Comment.class);
        assertNotNull(reply.getId());
        assertEquals(replyInput.getContent(), reply.getContent());
        Page<Comment> replies = apiGetPage("/comments/" + parentComment.getId() + "/comments", HttpStatus.OK, Comment.class, null);
        assertEquals(1, replies.getTotalElements());
        assertEquals(reply.getId(), replies.getContent().get(0).getId());
    }

    @Test
    void testGetComment() {
        Comment comment = generateTopLevelComment(generatePost(generateAccount()), generateAccount());
        Comment replyComment = generateReplyComment(comment, generateAccount());
        Comment fetchedComment = apiGet("/comments/" + comment.getId(), HttpStatus.OK, Comment.class);
        assertEquals(comment.getId(), fetchedComment.getId());
        assertEquals(comment.getContent(), fetchedComment.getContent());
        fetchedComment = apiGet("/comments/" + replyComment.getId(), HttpStatus.OK, Comment.class);
        assertEquals(replyComment.getId(), fetchedComment.getId());
        assertEquals(replyComment.getContent(), fetchedComment.getContent());
    }

    @Test
    @WithMockUser(username = TEST_USER_ID)
    void testUpdateComment() {
        Account account = getTestAccount();
        Comment comment = generateTopLevelComment(generatePost(account), account);
        Comment replyComment = generateReplyComment(comment, account);
        CommentInput updatedInput = new CommentInput("Updated comment content.");

        Comment updatedComment = apiPut("/comments/" + comment.getId(), updatedInput, HttpStatus.OK, Comment.class);
        assertEquals(comment.getId(), updatedComment.getId());
        assertEquals(updatedInput.getContent(), updatedComment.getContent());
        assertNotNull(updatedComment.getUpdatedAt());

        updatedComment = apiPut("/comments/" + replyComment.getId(), updatedInput, HttpStatus.OK, Comment.class);
        assertEquals(replyComment.getId(), updatedComment.getId());
        assertEquals(updatedInput.getContent(), updatedComment.getContent());
        assertNotNull(updatedComment.getUpdatedAt());

        Comment notMyReplyComment = generateReplyComment(comment, generateAccount());
        apiPut("/comments/" + notMyReplyComment.getId(), updatedInput, HttpStatus.FORBIDDEN, ApiError.class);

        CommentInput invalidInput = new CommentInput("");
        apiPut("/comments/" + comment.getId(), invalidInput, HttpStatus.BAD_REQUEST, ApiError.class);
    }

    @Test
    @WithMockUser(username = TEST_USER_ID)
    void testDeleteComment() {
        Account account = getTestAccount();
        Comment comment = generateTopLevelComment(generatePost(account), account);
        Comment replyComment = generateReplyComment(comment, account);

        // Refetch from public endpoint for good measure
        comment = apiGet("/comments/" + comment.getId(), HttpStatus.OK, Comment.class);

        apiDelete("/comments/" + comment.getId(), HttpStatus.NO_CONTENT, Void.class);
        apiDelete("/comments/" + comment.getId(), HttpStatus.NOT_FOUND, ApiError.class);
        var error = apiGet("/comments/" + comment.getId(), HttpStatus.NOT_FOUND, ApiError.class);
        assertApiErrorContains(error, "not found");

        // Ensure cascading delete functioned
        apiGet("/comments/" + replyComment.getId(), HttpStatus.NOT_FOUND, ApiError.class);

        // Can't delete someone else's comment
        Comment notMyComment = generateTopLevelComment(generatePost(generateAccount()), generateAccount());
        apiDelete("/comments/" + notMyComment.getId(), HttpStatus.FORBIDDEN, ApiError.class);
    }

    @Test
    void testGetComments() {
        Account account = generateAccount();
        Comment parentComment = generateTopLevelComment(generatePost(account), account);
        Set<UUID> ids = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            ids.add(generateReplyComment(parentComment, account).getId());
        }
        Page<Comment> repliesPage = apiGetPage("/comments/" + parentComment.getId() + "/comments", HttpStatus.OK, Comment.class, null);
        assertEquals(10, repliesPage.getTotalElements());
        repliesPage.forEach(reply -> ids.remove(reply.getId()));
        assertEquals(0, ids.size());
    }

}
