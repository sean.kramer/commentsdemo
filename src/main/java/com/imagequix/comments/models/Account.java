package com.imagequix.comments.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.imagequix.comments.models.base.TrackedEntity;
import io.hypersistence.utils.hibernate.type.basic.PostgreSQLCITextType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;

import java.util.UUID;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "account")
public class Account extends TrackedEntity {

    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    @JsonIgnore
    @Type(PostgreSQLCITextType.class)
    @Column(name = "email", nullable = false)
    private String email;

    @Column(nullable = false)
    private String name;

}
