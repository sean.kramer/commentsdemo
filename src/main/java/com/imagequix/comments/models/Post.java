package com.imagequix.comments.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.imagequix.comments.models.base.UpdateableTrackedEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "post")
public class Post extends UpdateableTrackedEntity {

    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    @Builder.Default
    @JsonProperty("reply_count")
    @Column(name = "reply_count", nullable = false)
    private int replyCount = 0;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "content", nullable = false)
    private String content;

    @Transient
    private List<Comment> comments;

}
