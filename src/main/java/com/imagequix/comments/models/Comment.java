package com.imagequix.comments.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.imagequix.comments.models.base.UpdateableTrackedEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "comment")
public class Comment extends UpdateableTrackedEntity {

    @Id
    @Builder.Default
    private UUID id = UUID.randomUUID();

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "postId", nullable = false)
    private Post post;

    @Builder.Default
    @Column(name = "depth", nullable = false, updatable = false)
    private int depth = 0;

    @Builder.Default
    @JsonProperty("reply_count")
    @Column(name = "reply_count", nullable = false)
    private int replyCount = 0;

    @ManyToOne
    @JoinColumn(name = "account_id", nullable = false)
    private Account account;

    @Column(name = "content", nullable = false)
    private String content;

    @JsonProperty("parent_id")
    @Column(updatable = false)
    private UUID parentId;

    @JsonProperty("post_id")
    @Column(updatable = false, insertable = false)
    private UUID postId;

    @Transient
    private List<Comment> comments;

    public void assembleReply(Comment reply) {
        if (comments == null) {
            comments = new ArrayList<>();
        }
        comments.add(reply);
    }

}
