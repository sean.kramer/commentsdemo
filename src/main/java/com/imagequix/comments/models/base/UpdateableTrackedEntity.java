package com.imagequix.comments.models.base;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Generated;
import org.hibernate.generator.EventType;

import java.time.Instant;

@Getter
@Setter
@MappedSuperclass
public abstract class UpdateableTrackedEntity extends TrackedEntity {

    @Generated(event = EventType.UPDATE)
    @Column(name = "updated_at", updatable = false, insertable = false)
    private Instant updatedAt;

}
