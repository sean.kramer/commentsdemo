package com.imagequix.comments.models.base;

import jakarta.persistence.MappedSuperclass;

/**
 * Generic abstract class that all @Entity classes should inherit from for ease of type checking.
 */
@MappedSuperclass
public abstract class BaseEntity {

}
