package com.imagequix.comments.auth;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.UUID;

/**
 * Very basic filter that naively looks for a Bearer token on the request header and, if present,
 * registers it on the security context.
 */
public class HeaderTokenFilter extends OncePerRequestFilter {

    public static final String BEARER_PREFIX = "Bearer ";

    @Override
    protected void doFilterInternal(
            HttpServletRequest request, HttpServletResponse response, FilterChain filterChain
    ) throws ServletException, IOException {
        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (authHeader != null && authHeader.startsWith(BEARER_PREFIX)) {
            String token = authHeader.substring(BEARER_PREFIX.length()).trim();
            if (!token.isEmpty()) {
                try {
                    UUID uuidToken = UUID.fromString(token);
                    var authToken = new UsernamePasswordAuthenticationToken(uuidToken, null);
                    SecurityContextHolder.getContext().setAuthentication(authToken);
                } catch (IllegalArgumentException e) {
                    // If the token is not a valid UUID, ignore it
                }
            }
        }
        // Regardless of if a token was found, proceed through the rest of the filter chain
        filterChain.doFilter(request, response);
    }

}
