package com.imagequix.comments.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;

import java.util.List;

public record ApiError(int status, List<String> errors) {

    public ApiError(HttpStatus httpStatus, List<String> errors) {
        this(httpStatus.value(), errors);
    }

    public ApiError(HttpStatus httpStatus, String message) {
        this(httpStatus.value(), List.of(message));
    }

    public ApiError(HttpStatusCode httpStatus, String message) {
        this(httpStatus.value(), List.of(message));
    }

}
