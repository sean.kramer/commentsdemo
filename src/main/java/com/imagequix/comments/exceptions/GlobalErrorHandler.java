package com.imagequix.comments.exceptions;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.HandlerMethodValidationException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.resource.NoResourceFoundException;

@ResponseBody
@RestControllerAdvice
public class GlobalErrorHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ApiError handleMethodValidationErrors(MethodArgumentNotValidException e) {
        var errors = e.getBindingResult().getAllErrors().stream().map(error ->
                "Field '" + ((FieldError) error).getField() + "' " + error.getDefaultMessage()).toList();
        e.printStackTrace();
        return new ApiError(HttpStatus.BAD_REQUEST, errors);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({HandlerMethodValidationException.class})
    public ApiError handleHandlerValidationErrors(HandlerMethodValidationException e) {
        var errors = e.getAllErrors().stream().map(error -> {
            String field = "unknown field";
            if (error.getArguments() != null && error.getArguments().length > 0 && error.getArguments()[0] instanceof DefaultMessageSourceResolvable) {
                field = ((DefaultMessageSourceResolvable) (error.getArguments()[0])).getDefaultMessage();
            }
            return "Field '" + field + "' " + error.getDefaultMessage();
        }).toList();
        e.printStackTrace();
        return new ApiError(HttpStatus.BAD_REQUEST, errors);
    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<ApiError> handleKnownResponseStatus(ResponseStatusException e) {
        e.printStackTrace();
        return ResponseEntity.status(e.getStatusCode()).body(new ApiError(e.getStatusCode(), e.getReason()));
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ApiError handleKnownResponseStatus(MethodArgumentTypeMismatchException e) {
        String message = e.getMessage();
        if (message.contains("Failed to convert")) {
            message = message.split("; ")[1];
        }
        e.printStackTrace();
        return new ApiError(HttpStatus.BAD_REQUEST, message);
    }

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ApiError handleKnownResponseStatus(HttpRequestMethodNotSupportedException e) {
        e.printStackTrace();
        return new ApiError(HttpStatus.METHOD_NOT_ALLOWED, e.getMessage());
    }

    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(NoResourceFoundException.class)
    public ApiError handleKnownResponseStatus(NoResourceFoundException e) {
        e.printStackTrace();
        return new ApiError(HttpStatus.METHOD_NOT_ALLOWED, e.getMessage());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiError handleAllExceptions(Exception e) {
        // TODO: Proper logging framework
        e.printStackTrace();
        return new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error");
    }

}
