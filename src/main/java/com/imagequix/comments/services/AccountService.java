package com.imagequix.comments.services;

import com.imagequix.comments.dtos.AccountInput;
import com.imagequix.comments.models.Account;
import com.imagequix.comments.repositories.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    public Account createAccount(AccountInput accountInput) {
        try {
            Account account = Account.builder()
                    .email(accountInput.getEmail())
                    .name(accountInput.getName())
                    .build();
            return accountRepository.save(account);
        } catch (Exception e) {
            if (e.getMessage().contains("duplicate key value violates unique constraint")) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "Account with email " + accountInput.getEmail() + " already exists");
            } else {
                throw e;
            }
        }
    }

    public void verifyAccess(Account accessHolder) {
        if (!getCurrent().getId().equals(accessHolder.getId())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Account does not have access to this resource");
        }
    }

    public Account getCurrent() {
        SecurityContext context = SecurityContextHolder.getContext();
        if (context.getAuthentication() instanceof UsernamePasswordAuthenticationToken auth) {
            UUID id = (auth.getPrincipal() instanceof UUID uuid) ? uuid : UUID.fromString(auth.getName());
            return accountRepository.findById(id)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid token provided"));
        }
        throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "No authorization provided");
    }

    public Account findOrThrow(UUID id) {
        return accountRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Account " + id + " not found"));
    }

    public Account findByEmail(String email) {
        return accountRepository.findByEmail(email)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Account with email " + email + " was not found"));
    }

}
