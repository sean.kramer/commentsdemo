package com.imagequix.comments.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.imagequix.comments.models.Comment;
import com.imagequix.comments.models.Post;
import com.imagequix.comments.repositories.CommentRepository;
import com.imagequix.comments.repositories.PostRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PostService {

    private final AccountService accountService;
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final ObjectMapper objectMapper;

    /**
     * Fetches and then assembles the full nested comment structure for a post
     */
    public Post getComplete(UUID id) {
        Post post = findOrThrow(id);
        post.setComments(commentRepository.findByPost_Id(id));
        assembleComments(post);
        return post;
    }

    public Page<Post> homePage(int page) throws JsonProcessingException {
        String response = postRepository.homePage(page);
        List<Post> posts;
        if (response == null) {
            posts = Collections.emptyList();
        } else {
            posts = objectMapper.readValue(response, new TypeReference<List<Post>>() {
            });
            posts.forEach(this::assembleComments);
        }
        return new PageImpl<>(posts, PageRequest.of(page, 20), postRepository.count());
    }

    public Post findOrThrow(UUID id) {
        return findOrThrow(id, false);
    }

    public Post fetchAndLock(UUID id) {
        return findOrThrow(id, true);
    }

    public Post findOrThrow(UUID id, boolean lockForWrite) {
        return (lockForWrite ? postRepository.findAndLock(id) : postRepository.findById(id))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Post " + id + " not found"));
    }

    public Post create(String title, String content) {
        return postRepository.save(Post.builder()
                .account(accountService.getCurrent())
                .title(title)
                .content(content)
                .build());
    }

    @Transactional
    public Post update(UUID id, String title, String content) {
        Post post = fetchAndLock(id);
        accountService.verifyAccess(post.getAccount());
        post.setTitle(title);
        post.setContent(content);
        return postRepository.save(post);
    }

    public void delete(UUID id) {
        Post post = findOrThrow(id); // Lock not needed for delete
        accountService.verifyAccess(post.getAccount());
        postRepository.delete(post);
    }

    private void assembleComments(Post post) {
        List<Comment> raw = post.getComments();
        List<Comment> topLevel = new ArrayList<>();
        HashMap<UUID, Comment> parents = new HashMap<>();
        HashMap<UUID, Comment> current = new HashMap<>();

        int i = 0;
        int depth = 0;
        for (Comment comment : raw) {
            if (comment.getDepth() == depth) {
                parents.put(comment.getId(), comment);
                topLevel.add(comment);
                i++;
            } else {
                depth++;
                break;
            }
        }

        for (; i < raw.size(); i++) {
            Comment comment = raw.get(i);
            if (comment.getDepth() != depth) {
                depth = comment.getDepth();
                parents = current;
                current = new HashMap<>();
            }
            Comment parent = parents.get(comment.getParentId());
            if (parent == null) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Parent not found for comment " + comment.getId());
            }
            parent.assembleReply(comment);
            current.put(comment.getId(), comment);
        }

        post.setComments(topLevel);
    }

}
