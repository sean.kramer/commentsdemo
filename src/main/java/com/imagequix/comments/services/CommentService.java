package com.imagequix.comments.services;

import com.imagequix.comments.models.Account;
import com.imagequix.comments.models.Comment;
import com.imagequix.comments.models.Post;
import com.imagequix.comments.repositories.CommentRepository;
import com.imagequix.comments.repositories.PostRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CommentService {

    private final AccountService accountService;

    private final CommentRepository commentRepository;

    private final PostService postService;
    private final PostRepository postRepository;

    public Comment findOrThrow(UUID id) {
        return findOrThrow(id, false);
    }

    private Comment fetchAndLock(UUID id) {
        return findOrThrow(id, true);
    }

    private Comment findOrThrow(UUID id, boolean lockForWrite) {
        return (lockForWrite ? commentRepository.findAndLock(id) : commentRepository.findById(id))
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Comment " + id + " not found"));
    }

    @Transactional
    public Comment create(UUID postId, UUID parentId, String content, Account account) {
        Comment parent = null;
        if (parentId != null) {
            parent = fetchAndLock(parentId);
            parent.setReplyCount(parent.getReplyCount() + 1);
            postId = parent.getPost().getId();
        }

        // We only need a write lock on the post if it's a direct reply
        Post post = postService.findOrThrow(postId, parent == null);
        if (parent == null) {
            post.setReplyCount(post.getReplyCount() + 1);
            postRepository.save(post);
        }

        return commentRepository.save(Comment.builder()
                .depth(parent == null ? 0 : parent.getDepth() + 1)
                .account(account)
                .post(post)
                .postId(postId) // Not used by hibernate, but makes sure value is on response
                .parentId(parent == null ? null : parent.getId())
                .content(content)
                .build());
    }

    @Transactional
    public Comment update(UUID id, String content) {
        Comment comment = fetchAndLock(id);
        accountService.verifyAccess(comment.getAccount());
        comment.setContent(content);
        return commentRepository.save(comment);
    }

    @Transactional
    public void delete(UUID id) {
        Comment comment = findOrThrow(id); // Lock not needed for delete
        accountService.verifyAccess(comment.getAccount());
        if (comment.getParentId() == null) {
            Post post = postService.fetchAndLock(comment.getPost().getId());
            post.setReplyCount(post.getReplyCount() - 1);
            postRepository.save(post);
        } else {
            Comment parent = fetchAndLock(comment.getParentId());
            parent.setReplyCount(parent.getReplyCount() - 1);
            commentRepository.save(parent);
        }

        commentRepository.delete(comment);
    }

}
