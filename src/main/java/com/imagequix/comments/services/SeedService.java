package com.imagequix.comments.services;

import com.imagequix.comments.models.Account;
import com.imagequix.comments.models.Comment;
import com.imagequix.comments.models.Post;
import com.imagequix.comments.repositories.AccountRepository;
import com.imagequix.comments.repositories.CommentRepository;
import com.imagequix.comments.repositories.PostRepository;
import jakarta.annotation.PostConstruct;
import jakarta.persistence.EntityManager;
import lombok.AllArgsConstructor;
import net.datafaker.Faker;
import org.hibernate.Session;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;

/**
 * Seeds database for testing
 */
@Service
@Profile("!test")
@AllArgsConstructor
public class SeedService {

    private final AccountRepository accountRepository;
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final EntityManager entityManager;
    private final Environment springEnvironment;

    private final Random random = new Random(0);
    private final Faker faker = new Faker(random);
    private final List<Account> accounts = new ArrayList<>(100);
    private final List<Comment> comments = new ArrayList<>();

    @PostConstruct
    protected void init() {
        if (Arrays.asList(springEnvironment.getActiveProfiles()).contains("clean")) {
            accountRepository.deleteAll();
            postRepository.deleteAll();
            commentRepository.deleteAll();
        }
        if (accountRepository.count() == 0) {
            System.out.println("Seeding Database... This takes up to a minute.");
            seed();
        }
    }

    private void seed() {
        // Open a stateless session to avoid caching for fast bulk inserts
        StatelessSession session = entityManager.unwrap(Session.class).getSessionFactory().openStatelessSession();
        Transaction tx = session.beginTransaction();

        List<Post> posts = new ArrayList<>(100);
        for (int i = 0; i < 100; i++) {
            String first = faker.name().firstName();
            String last = faker.name().lastName();
            String full = first + " " + last;
            Account account = Account.builder()
                    .id(UUID.nameUUIDFromBytes(full.getBytes()))
                    .email((first + "." + last + "@example.com").toLowerCase())
                    .name(full)
                    .build();
            accounts.add(account);
            // Give each account between 0 and 5 posts
            int postCount = random.nextInt(6);
            for (int j = 0; j <= postCount; j++) {
                String title = faker.lorem().sentence();
                Post post = Post.builder()
                        .id(UUID.nameUUIDFromBytes(title.getBytes()))
                        .account(account)
                        .title(title)
                        .content(faker.lorem().paragraph())
                        .build();
                posts.add(post);
                nestComments(post, null, 0);
            }
        }

        accounts.forEach(session::insert);
        posts.forEach(session::insert);
        comments.forEach(session::insert);

        tx.commit();
        session.close();
    }

    private void nestComments(Post post, Comment parent, int depth) {
        // Come up with a comment count that biases towards lower numbers but allows up to ~40
        double x = random.nextDouble();
        int commentCount = (int) (1.0 / (0.03 * Math.exp(10 * x)) - (5 * x) + 5);
        for (int i = 0; i <= commentCount; i++) {
            Account account = randomPoster();
            Comment comment = Comment.builder()
                    .id(UUID.randomUUID())
                    .depth(depth)
                    .account(account)
                    .post(post)
                    .parentId(parent == null ? null : parent.getId())
                    .content(faker.lorem().sentence())
                    .build();
            if (parent != null) {
                parent.setReplyCount(parent.getReplyCount() + 1);
            } else {
                post.setReplyCount(post.getReplyCount() + 1);
            }
            comments.add(comment);
            // 60% chance of adding new comments underneath, reduced at each depth
            if (random.nextDouble() < Math.pow(0.6, depth + 1.0)) {
                nestComments(post, comment, depth + 1);
            }
        }
    }

    private Account randomPoster() {
        return accounts.get(random.nextInt(accounts.size()));
    }

}
