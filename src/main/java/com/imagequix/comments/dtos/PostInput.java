package com.imagequix.comments.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PostInput {

    @NotBlank
    @Size(max = 140)
    private String title;

    @NotBlank
    @Size(max = 1000)
    private String content;

}
