package com.imagequix.comments.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.imagequix.comments.dtos.AccountInput;
import com.imagequix.comments.models.Account;
import com.imagequix.comments.models.Comment;
import com.imagequix.comments.models.Post;
import com.imagequix.comments.repositories.CommentRepository;
import com.imagequix.comments.repositories.PostRepository;
import com.imagequix.comments.services.AccountService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
@Tag(name = "Account Controller")
public class AccountController {

    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final AccountService accountService;
    private final ObjectMapper objectMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Create a new account", description = "Use the UUID of the created account as a bearer token to authenticate.")
    public Account createAccount(
            @RequestBody @Valid @NotNull AccountInput accountInput
    ) {
        return accountService.createAccount(accountInput);
    }

    @GetMapping("{email}")
    @Operation(summary = "Lookup an account by email", description = "Use the UUID of the returned account as a bearer token to authenticate.")
    @ApiResponse(responseCode = "200", description = "Account found")
    @ApiResponse(responseCode = "404", description = "Account not found")
    public Account lookupByEmail(
            @Parameter(description = "Email address to lookup the account", required = true) @Validated @Email @PathVariable String email
    ) {
        return accountService.findByEmail(email);
    }

    @GetMapping("{id}/posts")
    @Operation(summary = "Get posts by a specific account")
    public Page<Post> getAccountPosts(
            @ParameterObject @PageableDefault(size = 20, sort = "createdAt", direction = Sort.Direction.DESC) Pageable pageable,
            @Parameter(description = "Account ID", required = true) @PathVariable UUID id
    ) {
        return postRepository.findByAccount_Id(id, pageable);
    }

    @GetMapping("{id}/comments")
    @Operation(summary = "Get comments made by a specific account")
    public Page<Comment> getAccountComments(
            @ParameterObject @PageableDefault(size = 20, sort = "createdAt", direction = Sort.Direction.DESC) Pageable pageable,
            @Parameter(description = "Account ID", required = true) @PathVariable UUID id
    ) {
        return commentRepository.findByAccount_Id(id, pageable);
    }

}
