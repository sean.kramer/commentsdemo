package com.imagequix.comments.controllers;

import com.imagequix.comments.repositories.PostRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Health Controller")
@RestController
@RequiredArgsConstructor
public class HealthController {

    private final PostRepository postRepository;

    @GetMapping
    @Operation(summary = "Health Check", description = "Check if the API is online and the database is accessible")
    public String health() {
        return "API is Online. " + postRepository.count() + " posts in the database. " +
                "See <a href=/swagger-ui.html>here</a> for API documentation.";
    }

}
