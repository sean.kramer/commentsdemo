package com.imagequix.comments.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.imagequix.comments.dtos.CommentInput;
import com.imagequix.comments.dtos.PostInput;
import com.imagequix.comments.models.Comment;
import com.imagequix.comments.models.Post;
import com.imagequix.comments.repositories.CommentRepository;
import com.imagequix.comments.repositories.PostRepository;
import com.imagequix.comments.services.AccountService;
import com.imagequix.comments.services.CommentService;
import com.imagequix.comments.services.PostService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/posts")
@Tag(name = "Post Controller")
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;
    private final AccountService accountService;
    private final CommentRepository commentRepository;
    private final PostRepository postRepository;
    private final CommentService commentService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @SecurityRequirement(name = "bearerAuth")
    @Operation(summary = "Create a new post", description = "Creates a new post with the given title and content")
    @ApiResponse(responseCode = "201", description = "Successfully created the post")
    public Post createPost(
            @Parameter(description = "Post data transfer object", required = true)
            @RequestBody @Valid @NotNull PostInput postInput
    ) {
        return postService.create(postInput.getTitle(), postInput.getContent());
    }

    @GetMapping("{id}")
    @Operation(summary = "Get a specific post")
    public Post getPost(
            @Parameter(description = "The ID of the post to retrieve", required = true) @PathVariable UUID id
    ) {
        return postService.findOrThrow(id);
    }

    @GetMapping("{id}/complete")
    @Operation(summary = "Get a complete post including all nested comments",
            description = "This operation returns a post and all its nested comments, which might not scale well.")
    public Post getCompletePost(
            @Parameter(description = "The ID of the post to retrieve with all comments", required = true) @PathVariable UUID id
    ) {
        return postService.getComplete(id);
    }

    @PutMapping("{id}")
    @SecurityRequirement(name = "bearerAuth")
    @Operation(summary = "Update a specific post", description = "Updates the title and content of the specified post")
    @ApiResponse(responseCode = "200", description = "Successfully updated the post")
    public Post updatePost(
            @Parameter(description = "The ID of the post to update", required = true) @PathVariable UUID id,
            @Parameter(description = "Updated post data transfer object", required = true)
            @RequestBody @Valid @NotNull PostInput postInput
    ) {
        return postService.update(id, postInput.getTitle(), postInput.getContent());
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @SecurityRequirement(name = "bearerAuth")
    @Operation(summary = "Delete a specific post")
    @ApiResponse(responseCode = "204", description = "Successfully deleted the post")
    @ApiResponse(responseCode = "404", description = "Post not found")
    public void deletePost(
            @Parameter(description = "The ID of the post to delete", required = true) @PathVariable UUID id
    ) {
        postService.delete(id);
    }

    @GetMapping
    @Operation(summary = "List all posts")
    public Page<Post> getPosts(
            @ParameterObject @PageableDefault(size = 20, sort = "createdAt", direction = Sort.Direction.DESC) Pageable pageable
    ) {
        return postRepository.findAll(pageable);
    }

    @GetMapping("{id}/comments")
    @Operation(summary = "Fetch all direct replies to a specific post")
    public Page<Comment> getPostComments(
            @Parameter(description = "The ID of the post to retrieve comments from", required = true) @PathVariable UUID id,
            @ParameterObject @PageableDefault(size = 20, sort = "createdAt", direction = Sort.Direction.DESC) Pageable pageable
    ) {
        return commentRepository.findByPost_Id(id, pageable);
    }

    @PostMapping("{id}/comments")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Add a reply to a post", description = "Adds a new comment as a direct reply to a specified post")
    @ApiResponse(responseCode = "201", description = "Successfully added the reply")
    public Comment addReply(
            @Parameter(description = "The ID of the post to reply to", required = true) @PathVariable UUID id,
            @RequestBody @Valid @NotNull CommentInput commentInput
    ) {
        return commentService.create(id, null, commentInput.getContent(), accountService.getCurrent());
    }

    @GetMapping("home")
    @Operation(summary = "Home page posts", description = "Returns a page of posts for the home page, handling pagination internally")
    public Page<Post> homePage(
            @Parameter(description = "Page number to retrieve", example = "0") Integer page
    ) throws JsonProcessingException {
        if (page == null || page < 0) {
            page = 0;
        }
        return postService.homePage(page);
    }

}
