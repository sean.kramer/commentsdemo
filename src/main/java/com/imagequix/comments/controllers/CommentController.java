package com.imagequix.comments.controllers;

import com.imagequix.comments.dtos.CommentInput;
import com.imagequix.comments.models.Comment;
import com.imagequix.comments.repositories.CommentRepository;
import com.imagequix.comments.services.AccountService;
import com.imagequix.comments.services.CommentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/comments")
@RequiredArgsConstructor
@Tag(name = "Comment Controller")
public class CommentController {

    private final AccountService accountService;
    private final CommentService commentService;
    private final CommentRepository commentRepository;

    @PostMapping("{id}")
    @ResponseStatus(HttpStatus.CREATED)
    @SecurityRequirement(name = "bearerAuth")
    @Operation(summary = "Add a reply to an existing comment")
    public Comment addReply(
            @Parameter(description = "ID of the comment to reply to", required = true) @PathVariable UUID id,
            @Parameter(description = "Reply comment data transfer object", required = true)
            @RequestBody @Valid @NotNull CommentInput comment
    ) {
        return commentService.create(null, id, comment.getContent(), accountService.getCurrent());
    }

    @GetMapping("{id}")
    @Operation(summary = "View a specific comment")
    public Comment getComment(
            @Parameter(description = "ID of the comment to be viewed", required = true) @PathVariable UUID id
    ) {
        return commentService.findOrThrow(id);
    }

    @PutMapping("{id}")
    @SecurityRequirement(name = "bearerAuth")
    @Operation(summary = "Update an existing comment")
    public Comment updateComment(
            @Parameter(description = "ID of the comment to update", required = true) @PathVariable UUID id,
            @Parameter(description = "Updated comment data transfer object", required = true)
            @RequestBody @Valid @NotNull CommentInput commentInput
    ) {
        return commentService.update(id, commentInput.getContent());
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @SecurityRequirement(name = "bearerAuth")
    @Operation(summary = "Delete a specific comment")
    @ApiResponse(responseCode = "204", description = "Successfully deleted the comment")
    @ApiResponse(responseCode = "404", description = "Comment not found")
    public void deleteComment(
            @Parameter(description = "ID of the comment to delete", required = true) @PathVariable UUID id
    ) {
        commentService.delete(id);
    }

    @GetMapping("{id}/comments")
    @Operation(summary = "Get replies to a given parent comment")
    public Page<Comment> getComments(
            @Parameter(description = "ID of the parent comment to retrieve replies from", required = true) @PathVariable UUID id,
            @ParameterObject @PageableDefault(size = 20, sort = "createdAt", direction = Sort.Direction.DESC) Pageable pageable
    ) {
        return commentRepository.findByParentId(id, pageable);
    }

}
