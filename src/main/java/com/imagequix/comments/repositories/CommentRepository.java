package com.imagequix.comments.repositories;

import com.imagequix.comments.models.Comment;
import jakarta.persistence.LockModeType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, UUID> {

    Page<Comment> findByAccount_Id(UUID accountId, Pageable pageable);

    Page<Comment> findByPost_Id(UUID postId, Pageable pageable);

    Page<Comment> findByParentId(UUID parentId, Pageable pageable);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT c FROM Comment c WHERE c.id = :id")
    Optional<Comment> findAndLock(UUID id);

    List<Comment> findByPost_Id(UUID postId);

}
