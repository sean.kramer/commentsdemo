package com.imagequix.comments.repositories;

import com.imagequix.comments.models.Post;
import jakarta.persistence.LockModeType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface PostRepository extends JpaRepository<Post, UUID> {

    Page<Post> findByAccount_Id(UUID accountId, Pageable pageable);

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("SELECT p FROM Post p WHERE p.id = :id")
    Optional<Post> findAndLock(UUID id);

    @Query(nativeQuery = true, value = """
            select json_agg(row_to_json(posts))
            from (
                select p.*,
                    (
                        select json_agg(row_to_json(comments))
                        from (
                            select c.*,
                                json_build_object('name', a.name, 'id', a.id, 'created_at', a.created_at) as account
                            from comment c
                            join account a on c.account_id = a.id
                            where c.post_id = p.id
                            order by c.depth, c.created_at
                            limit 20
                        ) as comments
                    ) as comments,
                    json_build_object('name', a.name, 'id', a.id, 'created_at', a.created_at) as account
                from post p
                join account a on p.account_id = a.id
                order by p.created_at desc
                limit 20 offset :page * 20
            ) as posts;
            """)
    String homePage(int page);

}
