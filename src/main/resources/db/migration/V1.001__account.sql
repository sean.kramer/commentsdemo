create table account
(
    id         uuid primary key     default gen_random_uuid(),
    name       text        not null,
    email      citext      not null unique,
    created_at timestamptz not null default now()
);

create index on account (email);
