create table post
(
    id          uuid primary key default gen_random_uuid(),
    reply_count int              default 0,
    account_id  uuid          not null references account on delete cascade,
    title       varchar(140)  not null,
    content     varchar(1000) not null,
    created_at  timestamptz      default now(),
    updated_at  timestamptz
);

create trigger post_updated_at
    before update
    on post
    for each row
execute procedure set_updated_at();
