create table comment
(
    id          uuid primary key default gen_random_uuid(),
    parent_id   uuid references comment on delete cascade,
    post_id     uuid          not null references post on delete cascade,
    depth       int           not null,
    reply_count int              default 0,
    account_id  uuid          not null references account on delete cascade,
    content     varchar(1000) not null,
    created_at  timestamptz      default now(),
    updated_at  timestamptz
);

-- indexes for performance
create index on comment (parent_id);
create index on comment (post_id);
create index on comment (post_id, depth, created_at);

create trigger comment_updated_at
    before update
    on comment
    for each row
execute procedure set_updated_at();
