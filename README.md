<h3>Startup</h3>
To run the application, execute the following command in the root directory of the project:
`docker-compose up`

<h3>Inspect Database</h3>

The containerized PostgreSQL database can be connected to and inspected using the following credentials:\
**Base URL**: http://localhost:5433\
**Database name**: comments\
**Username**: admin\
**Password**: admin

<h3>API Documentation</h3>

View the API documentation at:\
http://localhost:3000/swagger-ui/index.html

<h3>Approach</h3>

Most endpoint use a paged, iterative approach to load more data.
If you want replies to a post or comment, you have to load them for that specific post or comment.
However, the `home` endpoint efficiently loads a more complete nested structure as a demo to a more nuanced approach.

<h3>Authoraization</h3>
To **create** posts or comments, you'll need to set a Bearer token in the Authorization header.
You can use the pre-seeded user `Jackie Rau` with the following token:\
`8d1e3937-9faf-3e92-8862-b465ed3e53e6`

Or you can create your own user by making a POST request to `/accounts`

To auth within Swagger, click on the `Authorize` button and enter the token
above or the `id` value returned from the new user input in the `Value` field.

If you want to **delete** or **update** posts or comments, you'll need to use the token of the user who created the post or comment.
That's available as the id of the `account` field of any post or comment.

<h3>Tests</h3>

Tests can be run using the following command *at the root of the project*:\
`docker run --rm -v ${pwd}/.:/app -w /app gradle:jdk21-alpine gradle test`\
(On Windows, use PowerShell instead of regular command prompt)

<h3>IntelliJ Setup</h3>

`Import Project From Existing Sources > Gradle` \
`File > Project Structure > Project > Project SDK > Corretto 21` \
(Use download SDK at bottom of dropdown if needed)
