FROM gradle:jdk21-alpine
WORKDIR /app
CMD ["gradle", "bootRun"]
